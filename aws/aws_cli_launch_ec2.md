## AWS Cloudshell 


VPC par défaut


- create key pair for SSH connection : 

```
aws ec2 create-key-pair --key-name ??? --query 'KeyMaterial' --output text > keyname.pem
```

- create security group : 

```
aws ec2 create-security-group --group-name ??? --description "My security group"
```

- add ssh to security group : 

```
aws ec2 authorize-security-group-ingress --group-name ??? --protocol tcp --port 22 --cidr 0.0.0.0/0
```

- create instance with Debian 11 AMI :

```
aws ec2 run-instances --image-id ami-016b30666f212275a --count 1 --instance-type t3.micro --key-name 333 --security-groups 333
```

- get important info 

```
aws ec2 describe-instances | grep PublicIpAddress && aws ec2 describe-instances | grep -m 1 PublicDnsName
```

- connect with ssh 

```
chmod 400 key.pem
ssh -i "key.pem" admin@ [publicDnsName]
```

