import re
import json
from pprint import pprint

ip_status_counts = {}

with open('access.log', 'r') as logfile:
    for line in logfile:
        match = re.search('^(\d+\.\d+\.\d+\.\d+)\s.*?\s(\d+)', line)

        if match:
            ip_address = match.group(1)
            status_code = match.group(2)

            if ip_address not in ip_status_counts:
                ip_status_counts[ip_address] = {}

            if status_code not in ip_status_counts[ip_address]:
                ip_status_counts[ip_address][status_code] = 0

            ip_status_counts[ip_address][status_code] += 1

for ip in ip_status_counts:
    total_requests = 0
    for status in ip_status_counts[ip]:
        total_requests += ip_status_counts[ip][status]
    ip_status_counts[ip]["requests"] = total_requests

print(json.dumps(ip_status_counts, indent=4))
