import csv
from csv import DictReader
from time import sleep
import win32com.client as client   # à installer après avoir installer Python dans le cmd
import pathlib

pj_path = pathlib.Path('dossier-refurb.pdf')     # nom complet du ficher en pièce-jointe (il doit être dans le même répertoire que le script)
pj_absolute = str(pj_path.absolute())

image_path = pathlib.Path('mysilogo.jpg')     # nom complet de l'image dans le corps du mail (il doit être dans le même repertoire que le script)
image_absolute = str(image_path.absolute())


# ouvre le fichier csv
with open('entrepriseregion.csv', 'r', newline='') as f:       # nom complet du fichier csv ou se trouve les adresses mails
    reader = csv.reader(f)
    distro = [row for row in reader]

# découpe la liste d'e-mails en morceaux de 30
    chunks = [distro[x:x+30] for x in range(0, len(distro), 30)]

# crée une instance d'outlook
    outlook = client.Dispatch('Outlook.Application')

# itere dans les morceaux crée un par un et envoie les mails
for chunk in chunks:
    # itere chaque adresse et envoie un mail
    for name, email in chunk:
        message = outlook.CreateItem(0)
        message.display()  # facultatif, s'en servir pour ouvrir et afficher un mail type outlook et s'assurer que tout est parfait avant d'envoyer
        message.To = email
        message.Subject = "MySI Refurb"  # objet du mail
        message.HTMLBody = ""
        html_body = """  e-mail body  """

        image = message.Attachments.Add(image_absolute)
        image.PropertyAccessor.SetProperty("http://schemas.microsoft.com/mapi/proptag/0x3712001F", "mysilogo")
        message.HTMLBody = html_body
        message.Attachments.Add(pj_absolute)
        message.Send()    # à désactiver pour ouvrir le template à envoyer avec message.Display()
                          # patiente 60 secs avant d'envoyer le prochain morceaux
        sleep(60)
