import os
import sys
import time
import shutil
import paramiko
import csv
from scp import SCPClient
from bs4 import BeautifulSoup


def main():
    get_file('192.168.20.254', '22', 'root', 'pfsense')

    backup()

    file_soup = create_soupe_des_familles()

    dict_from_xml = xml_to_list_dict(file_soup)
    dict_from_csv = csv_to_list_dict()

    check_duplicates(dict_from_xml, dict_from_csv)

    wifi_soup = edit_xml(file_soup, dict_from_csv)

    new_xml(wifi_soup)

    backup_current()

    new_upload('192.168.20.254', '22', 'root', 'pfsense')

    clear_cache('192.168.20.254', 'root', 'pfsense',
                r"tr -d '\r' < /conf/configg.xml > /conf/configgg.xml && rm /conf/config.xml && rm /conf/configg.xml && mv /conf/configgg.xml /conf/config.xml "
                r"&& rm /tmp/config.cache && /etc/rc.captiveportal_configure")


def get_file(hostname, port, username, password):
    ssh = paramiko.client.SSHClient()
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    ssh.connect(hostname, port, username, password)

    scp = SCPClient(ssh.get_transport())
    scp.get('/conf/config.xml')

    scp.close()

    print('fetching config file...done')


def backup():
    shutil.copyfile('config.xml', 'backup_ancien.xml')
    print('backup of current config file.....done')


def create_soupe_des_familles():
    infile = open('config.xml', 'r')
    contents = infile.read()
    file_soup = BeautifulSoup(contents, 'xml')

    return file_soup


def xml_to_list_dict(file_soup):
    soup_machines = file_soup.find("captiveportal").find("wifi_portal").find_all("passthrumac")

    result_list = []
    for machine in soup_machines:
        result_list.append({
            "descr": machine.find("descr").text,
            "mac": machine.find("mac").text

    return result_list


def csv_to_list_dict():
    my_file = open('mac_list.csv', 'r')
    reader = csv.DictReader(my_file)
    dict_from_csv = list(reader)
    return dict_from_csv


def check_duplicates(dict_from_xml, dict_from_csv):
    success = True
    for elem1 in dict_from_csv:
        for elem2 in dict_from_xml:
            if elem1['descr'] in elem2['descr'] and elem1['mac'] == elem2['mac']:
                print('Attention, ces descriptions et addresses existent déjà :', elem1['descr'], ':', elem1['mac'])
                success = False
            elif elem1['descr'] in elem2['descr']:
                print('Attention, ces descriptions existent déjà :', elem1['descr'], ':', elem1['mac'])
                success = False
            elif elem1['mac'] == elem2['mac']:
                print('Attention, ces addresses existent déjà:', elem1['descr'], ':', elem1['mac'])
                success = False

    if not success:
        print("Please correct input CSV and reload script !")
        os.system("pause")
        sys.exit()
    print("no duplicates...")


def edit_xml(file_soup, dict_from_csv):
    wifi_soup = file_soup.find("captiveportal").find("wifi_portal")

    for machine in dict_from_csv:
        machine_tag = file_soup.new_tag("passthrumac")

        action_tag = file_soup.new_tag("action")
        action_tag.string = "pass"

        mac_tag = file_soup.new_tag("mac")
        mac_tag.string = machine["mac"]

        description_tag = file_soup.new_tag("descr")
        description_tag.string = machine["descr"]

        machine_tag.append(action_tag)
        machine_tag.append(mac_tag)
        machine_tag.append(description_tag)

        wifi_soup.append(machine_tag)

    print(f"{len(dict_from_csv)} computers added...\n")

    return file_soup


def new_xml(file_soup):
    xml_content = str(file_soup)
    with open("config_result.xml", "w") as result_file:
        result_file.write(xml_content)

    print("creating new file...done")


def backup_current():
    os.remove('config.xml')
    shutil.copyfile('config_result.xml', 'backup_nouveau.xml')
    os.remove('config_result.xml')
    shutil.copyfile('backup_nouveau.xml', 'config.xml')

    print('backup of new config file.....done')


def new_upload(hostname, port, username, password):
    ssh = paramiko.client.SSHClient()
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    ssh.connect(hostname, port, username, password)
    scp = SCPClient(ssh.get_transport())
    scp.put('config.xml', '/conf/configg.xml')

    scp.close()

    print('uploading.....done')


def clear_cache(hostname, username, password, cmd_1):
    port = 22

    client = paramiko.client.SSHClient()
    client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    client.connect(hostname, port, username, password)

    stdin, stdout, stderr = client.exec_command(cmd_1)

    time.sleep(10)

    client.close()

    print('clearing cache.....done')
    print('reloading interface.....done')


if __name__ == "__main__":
    main()
