#!/usr/bin/env bash

# usage function provides a help message on how to use script 
# invoke if no or wrong arguments 
# $0 displays script name without having to hard code it

usage() {
  echo "Usage: $0 [-h] [-k KEY_NAME ] [-g SECURITY_GROUP_NAME ] [-b] [-l] "
  exit 1
}


create_instance_basic_install() {

aws ec2 create-key-pair --key-name $key_name --query 'KeyMaterial' --output text > $key_name.pem
aws ec2 create-security-group --group-name $group_name --description "My Security Group"
aws ec2 authorize-security-group-ingress --group-name $group_name --protocol tcp --port 22 --cidr 0.0.0.0/0
aws ec2 authorize-security-group-ingress --group-name $group_name --protocol tcp --port 80 --cidr 0.0.0.0/0
aws ec2 authorize-security-group-ingress --group-name $group_name --protocol tcp --port 443 --cidr 0.0.0.0/0
aws ec2 run-instances --image-id ami-016b30666f212275a --count 1 --instance-type t3.micro --key-name $key_name --security-groups $group_name --user-data file://basic_install.txt 
}

create_instance_lamp() {

aws ec2 create-key-pair --key-name $key_name --query 'KeyMaterial' --output text > $key_name.pem
aws ec2 create-security-group --group-name $group_name --description "My Security Group"
aws ec2 authorize-security-group-ingress --group-name $group_name --protocol tcp --port 22 --cidr 0.0.0.0/0
aws ec2 authorize-security-group-ingress --group-name $group_name --protocol tcp --port 80 --cidr 0.0.0.0/0
aws ec2 authorize-security-group-ingress --group-name $group_name --protocol tcp --port 443 --cidr 0.0.0.0/0
aws ec2 run-instances --image-id ami-016b30666f212275a --count 1 --instance-type t3.micro --key-name $key_name --security-groups $group_name --user-data file://install_lamp.txt 
}

# get_ops parses arguments
get_opts() {
  local opt OPTARG OPTIND
  while getopts "hk:g:bl" opt ; do
    case "$opt" in
      h) usage ;; 
      k) key_name="$OPTARG" ;;                            
      g) group_name="$OPTARG" ;;                          
      b) create_instance_basic_install ;;   
      l) create_instance_lamp ;;            

      \?) echo "ERROR: Invalid option -$OPTARG"
          usage ;;
    esac
  done
  shift $((OPTIND-1))
}

# sanity checking 

validate_opts() {
  if [ -z "$key_name" ] || [ -z "$group_name" ] ; then          
    echo "You must specify SSH key name with -k and Security Group Name with -g and choose a Debian configuration with -b (basic install) or -l (lamp with https) "
    usage
  fi
}

 
main() {

  export AWS_DEFAULT_OUTPUT="text" # var env instead of --output text for aws cli commands
  get_opts "$@"
  validate_opts
}

# guard clause, script only runs when executed, not sourced

if [ "$0" == "${BASH_SOURCE[0]}" ] ; then
  main "$@"
fi 
