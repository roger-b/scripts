#!/bin/bash


read -p 'Server name : ' servername
read -p 'Directory name : ' dirname

apt update -y
apt upgrade -y

apt install -y git  # wget curl net-tools tzdata ssh sudo screen zip nmap dnsutils locate ncdu

apt install -y apache2 php-common libapache2-mod-php php-cli mariadb-server

systemctl start apache2
systemctl enable apache2

a2enmod rewrite
a2enmod headers

systemctl restart apache2

mkdir -p /var/www/$dirname

chown -R www-data:www-data /var/www
usermod -a -G www-data admin

touch /etc/apache2/sites-available/$dirname.conf

cat << EOF > /etc/apache2/sites-available/$dirname.conf
<VirtualHost *:80>
    ServerAdmin rbeaumont@cefim.eu
    ServerName $servername
    DocumentRoot /var/www/$dirname
    <Directory /var/www/$dirname>
        Options -Indexes +FollowSymLinks -MultiViews
        AllowOverride all
        Order allow,deny
        Allow from All
    </Directory>
    ErrorLog ${APACHE_LOG_DIR}/error.log
    CustomLog ${APACHE_LOG_DIR}/access.log combined
</VirtualHost>
EOF

a2ensite $dirname.conf
a2dissite 000-default.conf
systemctl restart apache2

apt install -y snapd
snap install core
snap install --classic certbot

ln -s /snap/bin/certbot /usr/bin/certbot

sleep 30s
echo 'sleeping...'

certbot --apache --non-interactive --agree-tos -m rbeaumont@cefim.eu -d $servername

touch /var/www/$dirname/index.html
echo ' <html> <head> <h1> Hello from your system administrator :) </h1> <img src='https://i.imgur.com/YuIqiW9.jpeg'> </head> </html> ' > /var/www/$dirname/index.html

systemctl restart apache2
