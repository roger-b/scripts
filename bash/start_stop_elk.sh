#!/bin/bash

ES=$(systemctl is-active elasticsearch)      # echo Active ou Inactive
KI=$(systemctl is-active kibana)
LG=$(systemctl is-active logstash)

if [[ $ES != "active" ]]; then
    systemctl start elasticsearch > /dev/null 2>&1
        if [[ $? != 0 ]]
        then
        echo "ElasticSearch failed to start"
        else
        echo "ElasticSearch is active"
        fi
else
    systemctl stop elasticsearch
    echo "ElasticSearch is inactive"
fi

if [[ $KI != "active" ]]; then
    systemctl start kibana > /dev/null 2>&1
    if [[ $? != 0 ]]
    then
        echo "Kibana failed to start"
    else
        echo "Kibana is active"
    fi
else
    systemctl stop kibana
    echo "Kibana is now inactive"
fi

if [[ $LG != "active" ]]; then
    systemctl start logstash
    echo "Logstash failed to start"
else
    systemctl stop logstash
    echo "Logstash is now active"
fi
